// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Engine/TriggerVolume.h"

#include "Crystal.generated.h"	// Must be the LAST include in the header file


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class UE4_BUILDINGESCAPE_API UCrystal : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UCrystal();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

private:
	UPROPERTY(EditAnywhere)
	ATriggerVolume* CrystalTrigger = nullptr;

	AActor* Owner = nullptr;
};
