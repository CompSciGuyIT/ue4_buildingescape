// Fill out your copyright notice in the Description page of Project Settings.

#include "Crystal.h"


// Sets default values for this component's properties
UCrystal::UCrystal()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UCrystal::BeginPlay()
{
	Super::BeginPlay();

	// Find the owning actor
	Owner = GetOwner();
	if (!CrystalTrigger)
	{
		UE_LOG(LogTemp, Warning, TEXT("trigger volume is unavailable on %s"), *Owner->GetName())
	}
	
}


// Called every frame
void UCrystal::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

